package materialize

import (
	"fmt"
	"reflect"
	"sync"

	"github.com/influx6/faux/reflection"
	"gitlab.com/gokit/es"
)

// constant errors.,
const (
	ErrMustReturnBoolean           = Error("Function must return boolean value only")
	ErrTypesDoNotMatch             = Error("Function last value type must match return type")
	ErrTypeIsNotError              = Error("computed returned second value is not error type")
	ErrMustExpectLastValue         = Error("Function must expect previous value as argument")
	ErrMustReturnNewStateValue     = Error("Function must return new state value as return atleast")
	ErrInTwoReturnsLastMustBeError = Error("Function returning two values must return an error as second")
	ErrOnlyTwoReturnTypesAllowed   = Error("Function can only return two values types, where 1. latest value and 2. is error")
	ErrMustBePureFunction          = Error("Function must be a pure function receiving no input")
	ErrMustExpectTwoValues         = Error("Function must expect two values types")
	ErrMustExpectStateValue        = Error("Function must expect State value type")
	ErrMustExpectActionValue       = Error("Function must expect Action value type")
	ErrRequiresOtherFeeds          = Error("Feed requires other feeds to use")
	ErrMustBeSameType              = Error("Provided values must be same types")
	ErrMustMatchExpected           = Error("Function and Function arguments must match function type")
)

var (
	boolType   = reflect.TypeOf((*bool)(nil)).Elem()
	errorType  = reflect.TypeOf((*error)(nil)).Elem()
	stateType  = reflect.TypeOf((*State)(nil)).Elem()
	actionType = reflect.TypeOf((*Action)(nil)).Elem()
)

var (
	noAction Action
)

//*********************************************
// Error
//*********************************************

// Error implements a custom error type for package to
// provide consistency and constancy of type and value.
type Error string

func (e Error) Error() string {
	return string(e)
}

//*********************************************
// Feed and Types
//*********************************************

// State defines a map type which is used to store current
// state of existing values as key-value pairs.
type State map[string]interface{}

// Get returns the value of the giving key.
func (s State) Get(name string) interface{} {
	return s[name]
}

// Has returns true/false if giving key name exists.
func (s State) Has(name string) bool {
	_, ok := s[name]
	return ok
}

// Action defines an intent about an action to be performed
// by a Source against it's previous data to produce new data.
type Action struct {
	Type  string
	Value interface{}
}

// Computation defines an interface which exposes a method
// to perform a giving computation based on supplied previous
// value and Action.
type Computation interface {
	Compute(interface{}, Action) (interface{}, error)
}

// Feed defines a giving type of stream which captures it's
// last value and can be activated to update said value through
// a user defined computation optionally based on a Action.
type Feed interface {
	es.EventStream

	// Last provide an optimization method that skips the logic internally
	// for giving feed where feed may recompute it's value by letting users
	// retrieve last computed only.
	Last() interface{}

	// Compute must call internal logic with Action if required else ignore
	// provided action, returning any error received from computation.
	// Computed value will be set as last computed value for retrieval
	// through Last().
	Compute(Action) (interface{}, error)
}

//*********************************************
// BaseFeed
//*********************************************

var _ Feed = &BaseFeed{}

// BaseFeed implements the Feed interface based on a
// provided Computation and changed function. This
// is then used for internal logic necessary to
// both update last computed value and notification
// sending on new computed value.
//
// changed argument must match any of types:
//  -  func(former interface{}, latest interface{}) bool
//  -  func(former Type, latest Type) bool where Type is some custom type.
//
type BaseFeed struct {
	es.EventStream
	Computes Computation
	changed  func(former interface{}, latest interface{}) bool
	rw       sync.RWMutex
	last     interface{}
}

// Base returns a new BaseFeed based on provided Computation and changed
// function.
func Base(compute Computation, changed interface{}) (*BaseFeed, error) {
	var ch func(interface{}, interface{}) bool

	if changed != nil {
		if cho, ok := changed.(func(interface{}, interface{}) bool); ok {
			ch = cho
		} else {
			args, err := reflection.GetFuncArgumentsType(changed)
			if err != nil {
				return nil, err
			}

			returns, err := reflection.GetFuncReturnsType(changed)
			if err != nil {
				return nil, err
			}

			if err := changedPolicy(args, returns); err != nil {
				return nil, err
			}

			ch = makechanged(changed)
		}
	}

	var bn BaseFeed
	bn.changed = ch
	bn.Computes = compute
	bn.EventStream = es.New()
	return &bn, nil
}

// Compute runs the necessary internal logic to procure
// the latest value from feed's computation with provided
// Action as argument.
//
// Basic Rule:
//
// If the function to check changed state of value is provided
// and the returned value is different from previous value
// then new value will replace last value as last computed and
// a notification of changed is sent through event stream with
// latest value. Else if changed is denied then last value is
// returned and no notification is sent.
func (bf *BaseFeed) Compute(a Action) (interface{}, error) {
	bf.rw.RLock()
	before := bf.last
	bf.rw.RUnlock()

	latest, err := bf.Computes.Compute(before, a)
	if err != nil {
		return nil, err
	}

	// If we can validate changed and no new value then return previous.
	if bf.changed != nil && !bf.changed(before, latest) {
		return before, nil
	}

	// deliver notification to all listeners about changed.
	bf.Publish(latest)

	bf.rw.Lock()
	bf.last = latest
	bf.rw.Unlock()

	return latest, nil
}

// Last returns the last computed value, else nil.
func (bf *BaseFeed) Last() interface{} {
	var last interface{}
	bf.rw.RLock()
	last = bf.last
	bf.rw.RUnlock()
	return last
}

//*********************************************
// StaticFeed
//*********************************************

// Static returns a Feed who's computation consistently
// returns the same value. This will not exclude the normal
// operation of notifying update of value, though the value
// is constant but it's symantic to always get new value remains.
func Static(v interface{}, ch interface{}) (*BaseFeed, error) {
	return Pure(func() (interface{}, error) {
		return v, nil
	}, ch)
}

//*********************************************
// PureFeed
//*********************************************

var pureFunctionRules = []reflection.AreaValidation{
	baseReturnPolicy,
	func(arguments []reflect.Type, returns []reflect.Type) error {
		if len(arguments) != 0 {
			return ErrMustBePureFunction
		}
		return nil
	},
}

// Pure returns a new Feed using the provided function after
// ensuring it matches the expected standard else returning
// an error.
//
// PureFeeds can either never receive input or only
// ever receive the last state computed and nothing else.
//
// fn must be a Function Types in the ff:
//
// - func() (interface{}, error)
//
// - func() (Type, error) where Type is user defined or base type
//
func Pure(fn interface{}, changed interface{}) (*BaseFeed, error) {
	var pn pureFeed

	if fno, ok := fn.(func() (interface{}, error)); ok {
		pn.Fn = fno
	} else {
		args, err := reflection.GetFuncArgumentsType(fn)
		if err != nil {
			return nil, err
		}

		returns, err := reflection.GetFuncReturnsType(fn)
		if err != nil {
			return nil, err
		}

		if err := runRulesAgainst(pureFunctionRules, args, returns); err != nil {
			return nil, err
		}

		if len(returns) == 1 {
			pn.Fn = makePureWithSingleReturn(fn)
		}

		if len(returns) == 2 {
			pn.Fn = makePureWithNormalReturn(fn)
		}
	}

	return Base(pn, changed)
}

// pureFeed defines a type of Feed implementation, which response is a pure function
// that accepts no input from outside it self.
type pureFeed struct {
	Fn func() (interface{}, error)
}

func (pf pureFeed) Compute(_ interface{}, _ Action) (interface{}, error) {
	return pf.Fn()
}

//**************************************************************
// SeriesFeed: Feeds who's next value is affected by previous
//***************************************************************

var seriesPureFunctionRules = []reflection.AreaValidation{
	baseReturnPolicy,
	func(arguments []reflect.Type, returns []reflect.Type) error {
		if len(arguments) != 1 {
			return ErrMustMatchExpected
		}
		if reflection.IsStrictlyAssignableType(actionType, arguments[0]) || reflection.IsStrictlyAssignableType(stateType, arguments[0]) {
			return ErrMustExpectLastValue
		}
		if !reflection.IsSettableType(arguments[0], returns[0]) {
			return ErrTypesDoNotMatch
		}
		return nil
	},
}

// Series returns a new Feed using the provided function after
// ensuring it matches the expected standard else returning
// an error.
//
// Series Feeds are computation who's previous value
// play a role in it's next value.
//
// fn must be a Function Types in the ff:
//
// - func(interface{}) (interface{}, error)
//
// - func(Type) (Type, error) where Type is user defined or base type
//
func Series(fn interface{}, changed interface{}) (*BaseFeed, error) {
	var sn seriesFeed

	if fno, ok := fn.(func(interface{}) (interface{}, error)); ok {
		sn.Fn = fno
	} else {
		args, err := reflection.GetFuncArgumentsType(fn)
		if err != nil {
			return nil, err
		}

		returns, err := reflection.GetFuncReturnsType(fn)
		if err != nil {
			return nil, err
		}

		if err := runRulesAgainst(seriesPureFunctionRules, args, returns); err != nil {
			return nil, err
		}

		if len(returns) == 1 {
			sn.Fn = makeSeriesWithSingleReturn(fn)
		}

		if len(returns) == 2 {
			sn.Fn = makeSeriesWithNormalReturn(fn)
		}
	}

	return Base(sn, changed)
}

type seriesFeed struct {
	Fn func(interface{}) (interface{}, error)
}

func (sf seriesFeed) Compute(last interface{}, _ Action) (interface{}, error) {
	return sf.Fn(last)
}

//**************************************************************************
// ActionFeed: Feeds who's next value is affected by user provided action
//**************************************************************************

var actionFunctionRules = []reflection.AreaValidation{
	baseReturnPolicy,
	func(arguments []reflect.Type, returns []reflect.Type) error {
		if len(arguments) != 1 {
			return ErrMustMatchExpected
		}
		if !reflection.IsStrictlyAssignableType(actionType, arguments[0]) {
			return ErrMustExpectActionValue
		}
		return nil
	},
}

// PureAction returns a new Feed using the provided function after
// ensuring it matches the expected standard else returning
// an error.
//
// PureAction Feeds are computation who's latest value
// depends on incoming Action object.
//
// fn must be a Function Types in the ff:
//
// - func(Action) (interface{}, error)
//
// - func(Action) (Type, error) where Type is user defined or base type
//
func PureAction(fn interface{}, changed interface{}) (*BaseFeed, error) {
	var sn actionFeed

	if fno, ok := fn.(func(Action) (interface{}, error)); ok {
		sn.Fn = fno
	} else {
		args, err := reflection.GetFuncArgumentsType(fn)
		if err != nil {
			return nil, err
		}

		returns, err := reflection.GetFuncReturnsType(fn)
		if err != nil {
			return nil, err
		}

		if err := runRulesAgainst(actionFunctionRules, args, returns); err != nil {
			return nil, err
		}

		if len(returns) == 1 {
			sn.Fn = makeActionWithSingleReturn(fn)
		}

		if len(returns) == 2 {
			sn.Fn = makeActionWithNormalReturn(fn)
		}
	}

	return Base(sn, changed)
}

type actionFeed struct {
	Fn func(Action) (interface{}, error)
}

func (b actionFeed) Compute(_ interface{}, ac Action) (interface{}, error) {
	return b.Fn(ac)
}

//*******************************************************************************************
// SeriesActionFeed: Feeds who's next value is affected by both action and previous value
//*******************************************************************************************

var seriesActionFunctionRules = []reflection.AreaValidation{
	baseReturnPolicy,
	func(arguments []reflect.Type, returns []reflect.Type) error {
		if len(arguments) != 2 {
			return ErrMustMatchExpected
		}
		if reflection.IsStrictlyAssignableType(actionType, arguments[0]) || reflection.IsStrictlyAssignableType(stateType, arguments[0]) {
			return ErrMustExpectLastValue
		}
		if !reflection.IsSettableType(arguments[0], returns[0]) {
			return ErrTypesDoNotMatch
		}
		if !reflection.IsStrictlyAssignableType(actionType, arguments[1]) {
			return ErrMustExpectActionValue
		}
		return nil
	},
}

// SeriesAction returns a new Feed using the provided function after
// ensuring it matches the expected standard else returning
// an error.
//
// SeriesAction Feeds are computation who's latest value
// depends on incoming Action object and previous value.
//
// fn must be a Function Types in the ff:
//
// - func(interface{},Action) (interface{}, error)
//
// - func(Type, Action) (Type, error) where Type is user defined or base type
//
func SeriesAction(fn interface{}, changed interface{}) (*BaseFeed, error) {
	var sn seriesActionFeed

	if fno, ok := fn.(func(interface{}, Action) (interface{}, error)); ok {
		sn.Fn = fno
	} else {
		args, err := reflection.GetFuncArgumentsType(fn)
		if err != nil {
			return nil, err
		}

		returns, err := reflection.GetFuncReturnsType(fn)
		if err != nil {
			return nil, err
		}

		if err := runRulesAgainst(seriesActionFunctionRules, args, returns); err != nil {
			return nil, err
		}

		if len(returns) == 1 {
			sn.Fn = makeSeriesActionWithSingleReturn(fn)
		}

		if len(returns) == 2 {
			sn.Fn = makeSeriesActionWithNormalReturn(fn)
		}
	}

	return Base(sn, changed)
}

type seriesActionFeed struct {
	Fn func(interface{}, Action) (interface{}, error)
}

func (b seriesActionFeed) Compute(p interface{}, ac Action) (interface{}, error) {
	return b.Fn(p, ac)
}

//*******************************************************************************************
// Merge: Feeds who's next value is a calculation based on a series of other Feeds values
//*******************************************************************************************

var mutationFunctionRules = []reflection.AreaValidation{
	baseReturnPolicy,
	func(arguments []reflect.Type, returns []reflect.Type) error {
		if len(arguments) != 1 {
			return ErrMustMatchExpected
		}
		if !reflection.IsStrictlyAssignableType(stateType, arguments[0]) {
			return ErrMustExpectStateValue
		}
		return nil
	},
}

// Merge returns a new Feed using the provided function after
// ensuring it matches the expected standard else returning
// an error.
//
// Merge Feeds are computation who's latest value
// depends on merging other values from other feeds.
//
// fn must be a Function Types in the ff:
//
// - func(State) (interface{}, error)
//
// - func(State) (Type, error) where Type is user defined or base type
//
func Merge(fn interface{}, changed interface{}, others map[string]Feed) (*BaseFeed, error) {
	if others == nil || len(others) == 0 {
		return nil, ErrRequiresOtherFeeds
	}

	var sn mergeFeed
	sn.Others = others

	if fno, ok := fn.(func(State) (interface{}, error)); ok {
		sn.Fn = fno
	} else {
		args, err := reflection.GetFuncArgumentsType(fn)
		if err != nil {
			return nil, err
		}

		returns, err := reflection.GetFuncReturnsType(fn)
		if err != nil {
			return nil, err
		}

		if err := runRulesAgainst(mutationFunctionRules, args, returns); err != nil {
			return nil, err
		}

		if len(returns) == 1 {
			sn.Fn = makeMutationWithSingleReturn(fn)
		}

		if len(returns) == 2 {
			sn.Fn = makeMutationWithNormalReturn(fn)
		}
	}

	b, err := Base(sn, changed)
	if err != nil {
		return nil, err
	}

	for _, dep := range others {
		dep.Subscribe(func(_ interface{}) {
			b.Compute(noAction)
		})
	}

	return b, nil
}

type mergeFeed struct {
	Others map[string]Feed
	Fn     func(State) (interface{}, error)
}

func (b mergeFeed) Compute(_ interface{}, _ Action) (interface{}, error) {
	state := State{}
	for key, feed := range b.Others {
		state[key] = feed.Last()
	}
	return b.Fn(state)
}

//*******************************************************************************************************************
// SeriesMerge: Feeds who's next value is a calculation based on a series of other Feeds values and previous value
//*******************************************************************************************************************

var seriesMutationFunctionRules = []reflection.AreaValidation{
	baseReturnPolicy,
	func(arguments []reflect.Type, returns []reflect.Type) error {
		if len(arguments) != 2 {
			return ErrMustMatchExpected
		}
		if reflection.IsStrictlyAssignableType(actionType, arguments[0]) || reflection.IsStrictlyAssignableType(stateType, arguments[0]) {
			return ErrMustExpectLastValue
		}
		if !reflection.IsSettableType(arguments[0], returns[0]) {
			return ErrTypesDoNotMatch
		}
		if !reflection.IsStrictlyAssignableType(stateType, arguments[1]) {
			return ErrMustExpectStateValue
		}
		return nil
	},
}

// SeriesMerge returns a new Feed using the provided function after
// ensuring it matches the expected standard else returning
// an error.
//
// SeriesMerge Feeds are computation who's latest value
// depends on merging other values from other feeds.
//
// fn must be a Function Types in the ff:
//
// - func(interface{}, State) (interface{}, error)
//
// - func(Type, State) (Type, error) where Type is user defined or base type
//
func SeriesMerge(fn interface{}, changed interface{}, others map[string]Feed) (*BaseFeed, error) {
	if others == nil || len(others) == 0 {
		return nil, ErrRequiresOtherFeeds
	}

	var sn seriesMergeFeed
	sn.Others = others

	if fno, ok := fn.(func(interface{}, State) (interface{}, error)); ok {
		sn.Fn = fno
	} else {
		args, err := reflection.GetFuncArgumentsType(fn)
		if err != nil {
			return nil, err
		}

		returns, err := reflection.GetFuncReturnsType(fn)
		if err != nil {
			return nil, err
		}

		if err := runRulesAgainst(seriesMutationFunctionRules, args, returns); err != nil {
			return nil, err
		}

		if len(returns) == 1 {
			sn.Fn = makeSeriesMutationWithSingleReturn(fn)
		}

		if len(returns) == 2 {
			sn.Fn = makeSeriesMutationWithNormalReturn(fn)
		}
	}

	b, err := Base(sn, changed)
	if err != nil {
		return nil, err
	}

	for _, dep := range others {
		dep.Subscribe(func(_ interface{}) {
			b.Compute(noAction)
		})
	}

	return b, nil
}

type seriesMergeFeed struct {
	Others map[string]Feed
	Fn     func(interface{}, State) (interface{}, error)
}

func (b seriesMergeFeed) Compute(p interface{}, _ Action) (interface{}, error) {
	state := State{}
	for key, feed := range b.Others {
		state[key] = feed.Last()
	}
	return b.Fn(p, state)
}

//********************************************************************************************************************
// ActionMergeFeed: Feeds who's next value is a calculation based on a series of other values controlled by an action
//********************************************************************************************************************

var actionMergeFunctionRules = []reflection.AreaValidation{
	baseReturnPolicy,
	func(arguments []reflect.Type, returns []reflect.Type) error {
		if len(arguments) != 2 {
			return ErrMustMatchExpected
		}
		if !reflection.IsStrictlyAssignableType(stateType, arguments[0]) {
			return ErrMustExpectStateValue
		}
		if !reflection.IsStrictlyAssignableType(actionType, arguments[1]) {
			return ErrMustExpectActionValue
		}
		return nil
	},
}

// ActionMerge returns a new Feed using the provided function after
// ensuring it matches the expected standard else returning
// an error.
//
// SeriesMerge Feeds are computation who's latest value
// depends on merging other values from other feeds.
//
// fn must be a Function Types in the ff:
//
// - func(State, Action) (interface{}, error)
//
// - func(State, Action) (Type, error) where Type is user defined or base type
//
func ActionMerge(fn interface{}, changed interface{}, others map[string]Feed) (*BaseFeed, error) {
	if others == nil || len(others) == 0 {
		return nil, ErrRequiresOtherFeeds
	}

	var sn actionMergeFeed
	sn.Others = others

	if fno, ok := fn.(func(State, Action) (interface{}, error)); ok {
		sn.Fn = fno
	} else {
		args, err := reflection.GetFuncArgumentsType(fn)
		if err != nil {
			return nil, err
		}

		returns, err := reflection.GetFuncReturnsType(fn)
		if err != nil {
			return nil, err
		}

		if err := runRulesAgainst(actionMergeFunctionRules, args, returns); err != nil {
			return nil, err
		}

		if len(returns) == 1 {
			sn.Fn = makeCtrlStateWithSingleReturn(fn)
		}

		if len(returns) == 2 {
			sn.Fn = makeCtrlStateWithNormalReturn(fn)
		}
	}

	return Base(sn, changed)
}

type actionMergeFeed struct {
	Others map[string]Feed
	Fn     func(State, Action) (interface{}, error)
}

func (b actionMergeFeed) Compute(_ interface{}, ac Action) (interface{}, error) {
	state := State{}
	for key, feed := range b.Others {
		state[key] = feed.Last()
	}
	return b.Fn(state, ac)
}

//********************************************************************************************************************
// ActionMergeFeed: Feeds who's next value is a calculation based on a series of other values controlled by an action
//********************************************************************************************************************

var seriesActionMergeFunctionRules = []reflection.AreaValidation{
	baseReturnPolicy,
	func(arguments []reflect.Type, returns []reflect.Type) error {
		if len(arguments) != 3 {
			return ErrMustMatchExpected
		}
		if reflection.IsStrictlyAssignableType(actionType, arguments[0]) || reflection.IsStrictlyAssignableType(stateType, arguments[0]) {
			return ErrMustExpectLastValue
		}
		if !reflection.IsSettableType(arguments[0], returns[0]) {
			return ErrTypesDoNotMatch
		}
		if !reflection.IsStrictlyAssignableType(stateType, arguments[1]) {
			return ErrMustExpectStateValue
		}
		if !reflection.IsStrictlyAssignableType(actionType, arguments[2]) {
			return ErrMustExpectActionValue
		}
		return nil
	},
}

// SeriesActionMerge returns a new Feed using the provided function after
// ensuring it matches the expected standard else returning
// an error.
//
// SeriesMerge Feeds are computation who's latest value
// depends on merging other values from other feeds.
//
// fn must be a Function Types in the ff:
//
// - func(interface{}, State, Action) (interface{}, error)
//
// - func(Type, State, Action) (Type, error) where Type is user defined or base type
//
func SeriesActionMerge(fn interface{}, changed interface{}, others map[string]Feed) (*BaseFeed, error) {
	if others == nil || len(others) == 0 {
		return nil, ErrRequiresOtherFeeds
	}

	var sn seriesActionMergeFeed
	sn.Others = others

	if fno, ok := fn.(func(interface{}, State, Action) (interface{}, error)); ok {
		sn.Fn = fno
	} else {
		args, err := reflection.GetFuncArgumentsType(fn)
		if err != nil {
			return nil, err
		}

		returns, err := reflection.GetFuncReturnsType(fn)
		if err != nil {
			return nil, err
		}

		if err := runRulesAgainst(seriesActionMergeFunctionRules, args, returns); err != nil {
			return nil, err
		}

		if len(returns) == 1 {
			sn.Fn = makeCtrlMutationWithSingleReturn(fn)
		}

		if len(returns) == 2 {
			sn.Fn = makeCtrlMutationWithNormalReturn(fn)
		}
	}

	return Base(sn, changed)
}

type seriesActionMergeFeed struct {
	Others map[string]Feed
	Fn     func(interface{}, State, Action) (interface{}, error)
}

func (b seriesActionMergeFeed) Compute(p interface{}, ac Action) (interface{}, error) {
	state := State{}
	for key, feed := range b.Others {
		state[key] = feed.Last()
	}
	return b.Fn(p, state, ac)
}

//*********************************************
// internal functions
//*********************************************

func runRulesAgainst(rules []reflection.AreaValidation, args []reflect.Type, returns []reflect.Type) error {
	for _, rule := range rules {
		if err := rule(args, returns); err != nil {
			return err
		}
	}
	return nil
}

func makeCtrlMutationWithSingleReturn(fn interface{}) func(interface{}, State, Action) (interface{}, error) {
	return func(value interface{}, others State, req Action) (interface{}, error) {
		res, err := reflection.CallFunc(fn, req, value, others)
		if err != nil {
			return nil, err
		}

		return res[0], nil
	}
}

func makeCtrlMutationWithNormalReturn(fn interface{}) func(interface{}, State, Action) (interface{}, error) {
	return func(value interface{}, others State, req Action) (interface{}, error) {
		res, err := reflection.CallFunc(fn, req, value, others)
		if err != nil {
			return nil, err
		}

		if res[1] == nil {
			return res[0], nil
		}

		callErr, ok := res[1].(error)
		if !ok {
			return res[0], ErrTypeIsNotError
		}

		return res[0], callErr
	}
}

func makeSeriesActionWithSingleReturn(fn interface{}) func(interface{}, Action) (interface{}, error) {
	return func(value interface{}, req Action) (interface{}, error) {
		res, err := reflection.CallFunc(fn, value, req)
		if err != nil {
			return nil, err
		}

		return res[0], nil
	}
}

func makeSeriesActionWithNormalReturn(fn interface{}) func(interface{}, Action) (interface{}, error) {
	return func(value interface{}, req Action) (interface{}, error) {
		res, err := reflection.CallFunc(fn, value, req)
		if err != nil {
			return nil, err
		}

		if res[1] == nil {
			return res[0], nil
		}

		callErr, ok := res[1].(error)
		if !ok {
			return res[0], ErrTypeIsNotError
		}

		return res[0], callErr
	}
}

func makeSeriesMutationWithSingleReturn(fn interface{}) func(interface{}, State) (interface{}, error) {
	return func(value interface{}, others State) (interface{}, error) {
		res, err := reflection.CallFunc(fn, value, others)
		if err != nil {
			return nil, err
		}

		return res[0], nil
	}
}

func makeSeriesMutationWithNormalReturn(fn interface{}) func(interface{}, State) (interface{}, error) {
	return func(value interface{}, others State) (interface{}, error) {
		res, err := reflection.CallFunc(fn, value, others)
		if err != nil {
			return nil, err
		}

		if res[1] == nil {
			return res[0], nil
		}

		callErr, ok := res[1].(error)
		if !ok {
			return res[0], ErrTypeIsNotError
		}

		return res[0], callErr
	}
}

func makeCtrlStateWithSingleReturn(fn interface{}) func(State, Action) (interface{}, error) {
	return func(st State, act Action) (interface{}, error) {
		res, err := reflection.CallFunc(fn, act, st)
		if err != nil {
			return nil, err
		}

		return res[0], nil
	}
}

func makeCtrlStateWithNormalReturn(fn interface{}) func(State, Action) (interface{}, error) {
	return func(state State, act Action) (interface{}, error) {
		res, err := reflection.CallFunc(fn, act, state)
		if err != nil {
			return nil, err
		}

		if res[1] == nil {
			return res[0], nil
		}

		callErr, ok := res[1].(error)
		if !ok {
			return res[0], ErrTypeIsNotError
		}

		return res[0], callErr
	}
}

func makeMutationWithSingleReturn(fn interface{}) func(State) (interface{}, error) {
	return func(act State) (interface{}, error) {
		res, err := reflection.CallFunc(fn, act)
		if err != nil {
			return nil, err
		}

		return res[0], nil
	}
}

func makeMutationWithNormalReturn(fn interface{}) func(State) (interface{}, error) {
	return func(act State) (interface{}, error) {
		res, err := reflection.CallFunc(fn, act)
		if err != nil {
			return nil, err
		}

		if res[1] == nil {
			return res[0], nil
		}

		callErr, ok := res[1].(error)
		if !ok {
			return res[0], ErrTypeIsNotError
		}

		return res[0], callErr
	}
}

func makeActionWithSingleReturn(fn interface{}) func(Action) (interface{}, error) {
	return func(act Action) (interface{}, error) {
		res, err := reflection.CallFunc(fn, act)
		if err != nil {
			return nil, err
		}

		return res[0], nil
	}
}

func makeActionWithNormalReturn(fn interface{}) func(Action) (interface{}, error) {
	return func(act Action) (interface{}, error) {
		res, err := reflection.CallFunc(fn, act)
		if err != nil {
			return nil, err
		}

		if res[1] == nil {
			return res[0], nil
		}

		callErr, ok := res[1].(error)
		if !ok {
			return res[0], ErrTypeIsNotError
		}

		return res[0], callErr
	}
}

func makeSeriesWithSingleReturn(fn interface{}) func(interface{}) (interface{}, error) {
	return func(value interface{}) (interface{}, error) {
		res, err := reflection.CallFunc(fn, value)
		if err != nil {
			return nil, err
		}

		return res[0], nil
	}
}

func makeSeriesWithNormalReturn(fn interface{}) func(interface{}) (interface{}, error) {
	return func(value interface{}) (interface{}, error) {
		res, err := reflection.CallFunc(fn, value)
		if err != nil {
			return nil, err
		}

		if res[1] == nil {
			return res[0], nil
		}

		callErr, ok := res[1].(error)
		if !ok {
			return res[0], ErrTypeIsNotError
		}

		return res[0], callErr
	}
}

func makePureWithSingleReturn(fn interface{}) func() (interface{}, error) {
	return func() (interface{}, error) {
		res, err := reflection.CallFunc(fn)
		if err != nil {
			return nil, err
		}

		return res[0], nil
	}
}

func makePureWithNormalReturn(fn interface{}) func() (interface{}, error) {
	return func() (interface{}, error) {
		res, err := reflection.CallFunc(fn)
		if err != nil {
			return nil, err
		}

		if res[1] == nil {
			return res[0], nil
		}

		callErr, ok := res[1].(error)
		if !ok {
			return res[0], ErrTypeIsNotError
		}

		return res[0], callErr
	}
}

func makechanged(fn interface{}) func(interface{}, interface{}) bool {
	return func(former interface{}, latest interface{}) bool {
		fmt.Printf("Calling with: %+q %+q\n", former, latest)
		if res, err := reflection.CallFunc(fn, former, latest); err == nil {
			return res[0].(bool)
		}
		return false
	}
}

type fnClass int

const (
	noFnc                 fnClass = iota
	pureFnc                       // no arguments
	seriesFnc                     // last value only
	actionFnc                     // action only
	mutationFnc                   //  state only
	ctrlMutationFnc               // Action and State
	seriesActionFnc               // last value and Action
	seriesMutationFnc             // last Value and State
	seriesCtrlMutationFnc         // last Value, Action and State
)

func fnClassType(arguments []reflect.Type, returns []reflect.Type) fnClass {
	if len(arguments) == 0 {
		return pureFnc
	}
	if len(arguments) == 1 && !reflection.IsStrictlyAssignableType(actionType, arguments[0]) && !reflection.IsSettableType(
		arguments[0], stateType) {
		return seriesFnc
	}
	if len(arguments) == 1 && reflection.IsStrictlyAssignableType(actionType, arguments[0]) {
		return actionFnc
	}
	if len(arguments) == 1 && reflection.IsStrictlyAssignableType(stateType, arguments[0]) {
		return mutationFnc
	}
	if len(arguments) == 2 {
		if !reflection.IsStrictlyAssignableType(actionType, arguments[0]) && !reflection.IsStrictlyAssignableType(stateType, arguments[0]) {
			if reflection.IsStrictlyAssignableType(actionType, arguments[1]) {
				return seriesActionFnc
			}
			if reflection.IsStrictlyAssignableType(stateType, arguments[1]) {
				return seriesMutationFnc
			}
		}
		if reflection.IsStrictlyAssignableType(actionType, arguments[0]) && reflection.IsStrictlyAssignableType(stateType, arguments[1]) {
			return ctrlMutationFnc
		}
		return noFnc
	}
	if len(arguments) == 3 {
		if reflection.IsStrictlyAssignableType(actionType, arguments[0]) || reflection.IsStrictlyAssignableType(stateType, arguments[0]) {
			return noFnc
		}

		if reflection.IsStrictlyAssignableType(actionType, arguments[1]) && !reflection.IsStrictlyAssignableType(stateType, arguments[2]) {
			return seriesCtrlMutationFnc
		}
	}

	return noFnc
}

func changedPolicy(arguments []reflect.Type, returns []reflect.Type) error {
	if len(arguments) != 2 {
		return ErrMustExpectTwoValues
	}
	if len(returns) != 1 || reflection.IsSettableType(boolType, returns[0]) {
		return ErrMustReturnBoolean
	}
	if !reflection.IsSettableType(arguments[1], arguments[0]) {
		return ErrMustBeSameType
	}
	return nil
}

func baseReturnPolicy(arguments []reflect.Type, returns []reflect.Type) error {
	switch len(returns) {
	case 0:
		return ErrMustReturnNewStateValue
	case 1:
		// if it's a single value then it can not be
		// an error.
		if !reflection.IsSettableType(errorType, returns[0]) {
			return nil
		}
	case 2:
		// if we have a function returning two values, then
		// the last one must be an error type.
		if reflection.IsSettableType(errorType, returns[1]) {
			return nil
		}

		return ErrInTwoReturnsLastMustBeError
	}
	return ErrOnlyTwoReturnTypesAllowed
}

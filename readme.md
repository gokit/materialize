Materialized
-------------
Materialized strives to provide a stream-like propagation of values and value changes. It provides a computational based pattern based on user defined action objects and values which can be used to update provided structures with values, determine change to allow propagation of notification to interested parties.

It's a simple API with powerful concepts when combined into concept of materialized views and change propagation and immeutable structures.

Materialized functions are built for a given set of function format types and use reflect to allow users keep type safety in function definitions. Please refer to each documentation for each function in [Godoc](https://godoc.org/gitlab.com/gokit/materialized).

## Install

```bash
go get -u gitlab.com/gokit/materilize
```

## Change Determinism

Materialized leaves the concept of change to be determined by the user, this allows others define the rules governing what is determined to have changed

```go
materialize.Pure(func() (interface{}, error) {
	return "silkworm", nil
}, func(former string, latest string) bool {
	return former != latest
})
```

Above we see a materialized feed whoes change comparater simply checks inequality of string values.

## Types

Materialized supports the following types of compute patterns:

- Static

These are types which computation function consistently returns the provided value.

```go
materialize.Static("silkworm", nil)
```

- Pure

These are function that expects no input whatsoever to determine their output.

```go
materialize.Pure(func() (interface{}, error) {
	return "silkworm", nil
}, nil)
```

- Series

In materialized, the concept of `Series` is the notion that a previous change provides input to affect the next output. Hence a series is a computation whoes next value is a result of it's previous value.

```go
materialize.Series(func(former interface{}) (interface{}, error) {
	return "silkworm", nil
}, nil)
```

- PureAction

PureAction are functions that take in `materialized.Action` which contains data that affects the output to be returned.

```go
materialize.PureAction(func(act materialize.Action) (interface{}, error) {
	return "silkworm", nil
}, nil)
```

- SeriesAction

SeriesAction are functions who's value is the result of a materialized `Action` and previous value.

```go
materialize.SeriesAction(func(last interface{},act materialize.Action) (interface{}, error) {
	return "silkworm", nil
}, nil)
```

- Merge

Merge is a function who's value is the operation over a giving set of other Feed values provided as a key-value map, where the keys and feeds are provided by user.

```go
materialize.Merge(func(state materialize.State) (interface{}, error) {
	paint := state.Get("paint")
}, nil, map[string]materialized.Feed{
	"paint": Feed{},
})
```

- SeriesMerge

SeriesMerge is a function who's value is the operation of user provided feeds and former value.

```go
materialize.SeriesMerge(func(former interface{},state materialize.State) (interface{}, error) {
	paint := state.Get("paint")
}, nil, map[string]materialized.Feed{
	"paint": Feed{},
})
```

- ActionMerge

ActionMerge is a function who's next value is the operation of a `Action` and other feeds values.

```go
materialize.ActionMerge(func(state materialize.State, action materialized.Action) (interface{}, error) {
	paint := state.Get("paint")
}, nil, map[string]materialized.Feed{
	"paint": Feed{},
})
```

- SeriesActionMerge

SeriesActionMerge is a function who's next value is the operation it's former value, a `Action` and other feeds values in `State`.

```go
materialize.SeriesActionMerge(func(former interface{}, state materialize.State, action materialized.Action) (interface{}, error) {
	paint := state.Get("paint")
}, nil, map[string]materialized.Feed{
	"paint": Feed{},
})
```

package materialize_test

import (
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
	"gitlab.com/gokit/materialize"
)

func TestInvalidFeedFunction(t *testing.T) {
	base, err := materialize.Static("silkworm", nil)
	assert.NoError(t, err)

	var tests = []struct {
		Name  string
		Input []interface{}
	}{
		{
			Name: "Pure",
			Input: []interface{}{
				func(v string) interface{} { return nil },
				func(m interface{}) (interface{}, error) { return nil, nil },
				func(m, v interface{}) (interface{}, error) { return nil, nil },
				func(m, v int) (interface{}, error) { return nil, nil },
			},
		},
		{
			Name: "Series",
			Input: []interface{}{
				func() interface{} { return nil },
				func(m, v interface{}) (interface{}, error) { return nil, nil },
				func(m, v int) (interface{}, error) { return nil, nil },
				func(m interface{}, action materialize.Action) (interface{}, error) { return nil, nil },
			},
		},
		{
			Name: "PureAction",
			Input: []interface{}{
				func() interface{} { return nil },
				func(m, v interface{}) (interface{}, error) { return nil, nil },
				func(m, v int) (interface{}, error) { return nil, nil },
				func(m interface{}, action materialize.Action) (interface{}, error) { return nil, nil },
			},
		},
		{
			Name: "SeriesAction",
			Input: []interface{}{
				func() interface{} { return nil },
				func(m, v interface{}) (interface{}, error) { return nil, nil },
				func(m, v int) (interface{}, error) { return nil, nil },
				func(m interface{}, action materialize.State) (interface{}, error) { return nil, nil },
			},
		},
		{
			Name: "Merge",
			Input: []interface{}{
				func() interface{} { return nil },
				func(m, v interface{}) (interface{}, error) { return nil, nil },
				func(m, v int) (interface{}, error) { return nil, nil },
				func(m interface{}, action materialize.State) (interface{}, error) { return nil, nil },
			},
		},
		{
			Name: "SeriesMerge",
			Input: []interface{}{
				func() interface{} { return nil },
				func(m, v interface{}) (interface{}, error) { return nil, nil },
				func(m, v int) (interface{}, error) { return nil, nil },
				func(m interface{}, action materialize.Action) (interface{}, error) { return nil, nil },
			},
		},
		{
			Name: "ActionMerge",
			Input: []interface{}{
				func() interface{} { return nil },
				func(m, v interface{}) (interface{}, error) { return nil, nil },
				func(m, v int) (interface{}, error) { return nil, nil },
			},
		},
		{
			Name: "SeriesActionMerge",
			Input: []interface{}{
				func() interface{} { return nil },
				func(m, v interface{}) (interface{}, error) { return nil, nil },
				func(m, v int) (interface{}, error) { return nil, nil },
			},
		},
	}

	for _, tl := range tests {
		switch tl.Name {
		case "Pure":
			for _, v := range tl.Input {
				_, err := materialize.Pure(v, nil)
				assert.Error(t, err)
			}
		case "Series":
			for _, v := range tl.Input {
				_, err := materialize.Series(v, nil)
				assert.Error(t, err)
			}
		case "PureAction":
			for _, v := range tl.Input {
				_, err := materialize.PureAction(v, nil)
				assert.Error(t, err)
			}
		case "SeriesAction":
			for _, v := range tl.Input {
				_, err := materialize.SeriesAction(v, nil)
				assert.Error(t, err)
			}
		case "Merge":
			for _, v := range tl.Input {
				_, err := materialize.Merge(v, nil, map[string]materialize.Feed{"we": base})
				assert.Error(t, err)
			}
		case "SeriesMerge":
			for _, v := range tl.Input {
				_, err := materialize.SeriesMerge(v, nil, map[string]materialize.Feed{"we": base})
				assert.Error(t, err)
			}
		case "ActionMerge":
			for _, v := range tl.Input {
				_, err := materialize.ActionMerge(v, nil, map[string]materialize.Feed{"we": base})
				assert.Error(t, err)
			}
		case "SeriesActionMerge":
			for _, v := range tl.Input {
				_, err := materialize.SeriesActionMerge(v, nil, map[string]materialize.Feed{"we": base})
				assert.Error(t, err)
			}
		}
	}
}

func TestFeedChange(t *testing.T) {
	t.Logf("When no change comparator is provided")
	{
		base, err := materialize.PureAction(func(a materialize.Action) (interface{}, error) {
			return a.Value, nil
		}, nil)
		assert.NoError(t, err)

		defer base.Close()

		trigger := make(chan int, 1)
		base.Subscribe(func(i interface{}) {
			trigger <- 1
		})

		_, err = base.Compute(materialize.Action{Value: 1})
		assert.NoError(t, err)

		select {
		case <-trigger:
		case <-time.After(10 * time.Millisecond):
			assert.Fail(t, "should have received notification")
		}

		assert.Equal(t, base.Last(), 1)
	}
	t.Logf("When changed comparator is provided")
	{
		base, err := materialize.PureAction(func(a materialize.Action) (interface{}, error) {
			return a.Value, nil
		}, func(former, latest interface{}) bool {
			return false
		})
		assert.NoError(t, err)

		defer base.Close()

		trigger := make(chan int, 1)
		base.Subscribe(func(i interface{}) {
			trigger <- 1
		})

		_, err = base.Compute(materialize.Action{Value: 1})
		assert.NoError(t, err)

		select {
		case <-trigger:
			assert.Fail(t, "should have not received notification")
		case <-time.After(10 * time.Millisecond):
		}

		assert.Equal(t, base.Last(), nil)
	}
}

func TestFeed(t *testing.T) {
	base, err := materialize.Static("silkworm", nil)
	assert.NoError(t, err)

	var tests = []struct {
		Name string
		Feed materialize.Feed
	}{
		{
			Name: "Static",
			Feed: must(t, func() (*materialize.BaseFeed, error) {
				return materialize.Static("silkworm", nil)
			}),
		},
		{
			Name: "Pure",
			Feed: must(t, func() (*materialize.BaseFeed, error) {
				return materialize.Pure(func() (interface{}, error) {
					return "silkworm", nil
				}, nil)
			}),
		},
		{
			Name: "Series",
			Feed: must(t, func() (*materialize.BaseFeed, error) {
				return materialize.Series(func(v interface{}) (interface{}, error) {
					return "silkworm", nil
				}, nil)
			}),
		},
		{
			Name: "PureAction",
			Feed: must(t, func() (*materialize.BaseFeed, error) {
				return materialize.PureAction(func(v materialize.Action) (interface{}, error) {
					return "silkworm", nil
				}, nil)
			}),
		},
		{
			Name: "SeriesAction",
			Feed: must(t, func() (*materialize.BaseFeed, error) {
				return materialize.SeriesAction(func(v interface{}, a materialize.Action) (interface{}, error) {
					return "silkworm", nil
				}, nil)
			}),
		},
		{
			Name: "Merge",
			Feed: must(t, func() (*materialize.BaseFeed, error) {
				return materialize.Merge(func(a materialize.State) (interface{}, error) {
					return "silkworm", nil
				}, nil, map[string]materialize.Feed{"day": base})
			}),
		},
		{
			Name: "SeriesMerge",
			Feed: must(t, func() (*materialize.BaseFeed, error) {
				return materialize.SeriesMerge(func(v interface{}, a materialize.State) (interface{}, error) {
					return "silkworm", nil
				}, nil, map[string]materialize.Feed{"day": base})
			}),
		},
		{
			Name: "ActionMerge",
			Feed: must(t, func() (*materialize.BaseFeed, error) {
				return materialize.ActionMerge(func(a materialize.State, ac materialize.Action) (interface{}, error) {
					return "silkworm", nil
				}, nil, map[string]materialize.Feed{"day": base})
			}),
		},
		{
			Name: "SeriesActionMerge",
			Feed: must(t, func() (*materialize.BaseFeed, error) {
				return materialize.SeriesActionMerge(func(v interface{}, a materialize.State, ac materialize.Action) (interface{}, error) {
					return "silkworm", nil
				}, nil, map[string]materialize.Feed{"day": base})
			}),
		},
	}

	for _, tl := range tests {
		t.Logf("Feed: %q", tl.Name)
		{
			computeTestSuite(t, tl.Feed)
		}
	}
}

func must(t *testing.T, fn func() (*materialize.BaseFeed, error)) materialize.Feed {
	v, err := fn()
	assert.NoError(t, err)
	return v
}

func mustFail(t *testing.T, fn func() (*materialize.BaseFeed, error)) materialize.Feed {
	_, err := fn()
	assert.Error(t, err)
	return nil
}

func computeTestSuite(t *testing.T, feed materialize.Feed) {
	assert.Nil(t, feed.Last())

	latest, err := feed.Compute(materialize.Action{
		Type:  "COMPUTE",
		Value: "silkworm",
	})

	assert.NoError(t, err)
	assert.NotNil(t, latest)
	assert.Equal(t, latest, feed.Last())
}
